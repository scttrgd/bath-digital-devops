from django.test import TestCase
from attendees.models import Attendee
from django.http import HttpResponse
from django.urls import reverse
from django.contrib import auth
from django.test import Client



class AttendeeModelTests(TestCase):
    def setUp(self):
        attendee = Attendee.objects.create(first_name='Maeve', last_name='Millay', company='Delos Corporation', attended=False)
        attendee.save()

    def test_attendee(self):
        attendee = Attendee.objects.get(first_name='Maeve')
        self.assertEqual(attendee.attended, False)

   
