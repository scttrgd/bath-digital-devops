# Pull base image, python is based on Alpine
FROM python:3.7

# Set environment varibles
# Stops python from writing out a .PYC when installing dependencies 
ENV PYTHONDONTWRITEBYTECODE 1
# Stops python log messages from being buffered 
ENV PYTHONUNBUFFERED 1
# This is to get around using heroku's postgres db in our container
ENV DATABASE_URL sqlite:////db.sqlite3

# Set the working directory (map local directory to something inside the container)  
WORKDIR /code
COPY . /code/
# Install dependencies 
RUN pip install --upgrade pip
RUN pip install pipenv
RUN pip install -r requirements.txt
RUN python manage.py migrate

# Specify the port that we want exposed to the host system
EXPOSE 8000


CMD [ "python", "manage.py", "runserver", "0.0.0.0:8000" ]

