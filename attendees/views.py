from django.shortcuts import render
from .models import Attendee

def attendee_list(request):
    attendees = Attendee.objects.all()
    return render(request, 'attendees/attendee_list.html', {'attendees': attendees})

# Create your views here.
