from django.db import models

class Attendee(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    company = models.CharField(max_length=200)
    attended = models.BooleanField(default=False)
    def __str__(self):
        return self.first_name

# Create your models here.
