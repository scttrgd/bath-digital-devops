from django.urls import path
from . import views

urlpatterns = [
        path('', views.attendee_list, name='attendee_list'),
]

